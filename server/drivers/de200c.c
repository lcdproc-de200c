/*
 * Driver for Noritake VFD in the HP DE200C
 *
 * Copyright (C) 2008 Christophe de Dinechin
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "lcd.h"
#include "lcd_lib.h"

#define VFDCLIENT               "vfddisplay"
#define VFDOPTIONS              "de200c-vfd/clients/options.tbl"
#define VFDCONFIG               "lcdproc"

#include "de200c-vfd/clients/vfdclient.h"
#include "report.h"

// ============================================================================
// 
// Vars for the server core
// 
// ============================================================================

MODULE_EXPORT char *api_version = API_VERSION;
MODULE_EXPORT int stay_in_foreground = 0;
MODULE_EXPORT int supports_multiple = 0;
MODULE_EXPORT char *symbol_prefix = "de200c_";

// ============================================================================
// 
//   Drivers entry points
// 
// ============================================================================

typedef struct PrivateData
// ----------------------------------------------------------------------------
//   Private data used by the driver
// ----------------------------------------------------------------------------
{
    const char *server;
    int port;
    int vfd;
    const char *config;
    vfd_configuration cfg;
    int line, column;
    int hasBigNum;
} PrivateData;

int char_width = 6;
int char_height = 8;

#define X(_x_)    (p->cfg.x + ((_x_) - 1) * char_width)
#define Y(_y_)    (p->cfg.y + ((_y_) - 1) * char_height)

char HeartBeat[8*6] =
// ----------------------------------------------------------------------------
//   Heart beat pattern
// ----------------------------------------------------------------------------
{
    0x06, 0x19, 0xE2, 0x19, 0x06, 0x00,
    0x06, 0x1F, 0xFE, 0x1F, 0x06, 0x00,
    0x06, 0x1F, 0xFE, 0x1F, 0x06, 0x00,
    0x06, 0x1F, 0xFE, 0x1F, 0x06, 0x00,
    0x00, 0x06, 0x1E, 0x06, 0x00, 0x00,
    0x00, 0x00, 0x0C, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};


// ============================================================================
// 
//   Big digits are cool
// 
// ============================================================================
//   These were obtained by hand-editing a 1-bit .xbm file, 18 pixel wide,
//   mirrorred left-right and inverted to match the pattern order for the VFD

static char big0[] =
// ----------------------------------------------------------------------------
//   Pattern for 0
// ----------------------------------------------------------------------------
{
   0x00, 0xFF, 0x01,
   0xE0, 0xFF, 0x0F,
   0xF0, 0xFF, 0x1F,
   0xF8, 0xFF, 0x3F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0x04, 0x00, 0x40,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xF8, 0xFF, 0x3F,
   0xF0, 0xFF, 0x1F,
   0xE0, 0xFF, 0x0F,
   0x00, 0xFF, 0x01
};

static char big1[] =
// ----------------------------------------------------------------------------
//   Pattern for 1
// ----------------------------------------------------------------------------
{
   0xC0, 0x00, 0x00,
   0xC0, 0x00, 0x60,
   0xE0, 0xFF, 0x7F,
   0xF0, 0xFF, 0x7F,
   0xF8, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0x00, 0x00, 0x60
};

static char big2[] =
// ----------------------------------------------------------------------------
//   Pattern for 2
// ----------------------------------------------------------------------------
{
   0xE0, 0x00, 0x78,
   0xF8, 0x01, 0x7C,
   0xF8, 0x01, 0x7E,
   0xFC, 0x81, 0x7F,
   0xFC, 0xC0, 0x7B,
   0x04, 0xE0, 0x79,
   0x0C, 0xF8, 0x78,
   0xFC, 0xFF, 0x78,
   0xFC, 0x7F, 0x78,
   0xF8, 0x3F, 0x78,
   0xF8, 0x1F, 0x7E,
   0xF0, 0x0F, 0x7E,
   0xE0, 0x03, 0x0E
};

static char big3[] =
// ----------------------------------------------------------------------------
//   Pattern for 3
// ----------------------------------------------------------------------------
{
   0xE0, 0x00, 0x0E,
   0xF8, 0x01, 0x3F,
   0xF8, 0x11, 0x3F,
   0xFC, 0x11, 0x7E,
   0x04, 0x10, 0x40,
   0x04, 0x10, 0x40,
   0x0C, 0x38, 0x60,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xEF, 0x3F,
   0xF8, 0xEF, 0x3F,
   0xF0, 0xC7, 0x1F,
   0xE0, 0x83, 0x0F
};

static char big4[] =
// ----------------------------------------------------------------------------
//   Pattern for 4
// ----------------------------------------------------------------------------
{
   0x00, 0x00, 0x02,
   0x00, 0x00, 0x03,
   0x00, 0xC0, 0x03,
   0x00, 0xF0, 0x02,
   0x00, 0x3C, 0x02,
   0x00, 0x0E, 0x62,
   0x80, 0xFF, 0x7F,
   0xC0, 0xFF, 0x7F,
   0xF0, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0xFF, 0x7F,
   0x00, 0x00, 0x62,
   0x00, 0x00, 0x02
};

static char big5[] =
// ----------------------------------------------------------------------------
//   Pattern for 5
// ----------------------------------------------------------------------------
{
   0x00, 0x00, 0x0E,
   0x80, 0x1F, 0x3F,
   0xFC, 0x1F, 0x3F,
   0xFC, 0x1F, 0x7F,
   0x3C, 0x0E, 0x7E,
   0x3C, 0x06, 0x40,
   0x3C, 0x0E, 0x60,
   0x3C, 0xFE, 0x7F,
   0x3C, 0xFE, 0x7F,
   0x3C, 0xFC, 0x3F,
   0x3C, 0xFC, 0x1F,
   0x00, 0xF8, 0x0F,
   0x00, 0xE0, 0x07
};

static char big6[] =
// ----------------------------------------------------------------------------
//   Pattern for 6
// ----------------------------------------------------------------------------
{
   0x00, 0xFE, 0x03,
   0xC0, 0xFF, 0x0F,
   0xE0, 0xFF, 0x3F,
   0xF0, 0xFF, 0x3F,
   0xF8, 0xFF, 0x7F,
   0xF8, 0xFF, 0x7F,
   0x1C, 0x03, 0x40,
   0x0C, 0xFF, 0x7F,
   0x0C, 0xFF, 0x7F,
   0x04, 0xFF, 0x3F,
   0x04, 0xFE, 0x3F,
   0x04, 0xFC, 0x0F,
   0x00, 0xF0, 0x03
};

static char big7[] =
// ----------------------------------------------------------------------------
//   Pattern for 7
// ----------------------------------------------------------------------------
{
   0xFC, 0x00, 0x00,
   0xFC, 0x00, 0x00,
   0xFC, 0x00, 0x00,
   0x3C, 0x00, 0x60,
   0x3C, 0x00, 0x7E,
   0x3C, 0xE0, 0x7F,
   0x3C, 0xF8, 0x7F,
   0x3C, 0xFE, 0x7F,
   0xBC, 0xFF, 0x7F,
   0xFC, 0x1F, 0x60,
   0xFC, 0x01, 0x00,
   0x3C, 0x00, 0x00,
   0x0C, 0x00, 0x00
};

static char big8[] =
// ----------------------------------------------------------------------------
//   Pattern for 8
// ----------------------------------------------------------------------------
{
   0xE0, 0x83, 0x0F,
   0xF0, 0xC7, 0x1F,
   0xF8, 0xEF, 0x3F,
   0xFC, 0xFF, 0x7F,
   0xFC, 0x7F, 0x60,
   0x8C, 0x7F, 0x40,
   0x04, 0xFF, 0x40,
   0x04, 0xFE, 0x41,
   0x0C, 0xFE, 0x63,
   0xFC, 0xFF, 0x3F,
   0xF8, 0xFF, 0x3F,
   0xF0, 0xF3, 0x1F,
   0xE0, 0xC1, 0x07
};

static char big9[] =
// ----------------------------------------------------------------------------
//   Pattern for 9
// ----------------------------------------------------------------------------
{
   0x80, 0x3F, 0x00,
   0xE0, 0x7F, 0x40,
   0xF8, 0xFF, 0x40,
   0xF8, 0xFF, 0x41,
   0xFC, 0xFF, 0x61,
   0xFC, 0xFF, 0x61,
   0x04, 0x80, 0x79,
   0xFC, 0xFF, 0x3F,
   0xFC, 0xFF, 0x3F,
   0xF8, 0xFF, 0x1F,
   0xF8, 0xFF, 0x0F,
   0xF0, 0xFF, 0x07,
   0x80, 0xFF, 0x00
};

static char big10[] =
// ----------------------------------------------------------------------------
//   Pattern for : (digit 10 in lcdproc)
// ----------------------------------------------------------------------------
{
   0xC0, 0x01, 0x07,
   0xE0, 0x83, 0x0F,
   0xE0, 0x83, 0x0F,
   0xE0, 0x83, 0x0F,
   0xC0, 0x01, 0x07,
};


MODULE_EXPORT int
de200c_init (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Initialize the connection to the DE200c VFD server
// ----------------------------------------------------------------------------
{
    PrivateData *p;
    int w, h;

    /* Allocate and store private data */
    p = (PrivateData *) calloc(1, sizeof(PrivateData));
    if (p == NULL)
        return -1;
    if (drvthis->store_private_ptr(drvthis, p))
        return -1;

    /* initialize private data */
    p->server = drvthis->config_get_string(drvthis->name, "Server", 0, server);
    p->port = drvthis->config_get_int(drvthis->name, "Port", 0, port);
    p->vfd = vfd_open(server, port);
    if (p->vfd < 0)
        return -1;

    // Set display sizes
    p->config = drvthis->config_get_string(drvthis->name, "Config", 0, cfg);
    p->cfg = vfd_configure(p->vfd, (char *) p->config);
    vfd_select_font(p->vfd, 0); // Monospace font
    for (h = 0; h < 8; h++)
        vfd_define_glyph(p->vfd, h+128, 6, 1, HeartBeat + 6 * h);
    
    w = drvthis->request_display_width();
    if (w > 0 && w * char_width <= p->cfg.w)
        p->cfg.w = w * char_width;
    h = drvthis->request_display_height();
    if (h > 0 && h * char_height <= p->cfg.h)
        p->cfg.h = h * char_height;
    p->line = 0;
    p->column = 0;
    p->hasBigNum = 0;
        
    report(RPT_DEBUG, "%s: init() done", drvthis->name);

    return 1;
}


MODULE_EXPORT void
de200c_close (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Close the device. The server will drop us when the socket closes
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;

    if (p != NULL) {
        free(p);
    }	
    drvthis->store_private_ptr(drvthis, NULL);
}


MODULE_EXPORT int
de200c_width (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Return display width (in characters)
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    return p->cfg.w / char_width;
}


MODULE_EXPORT int
de200c_height (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Returns the display height
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    return p->cfg.h / char_height;
}


MODULE_EXPORT void
de200c_clear (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Clear the LCD screen
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    vfd_clear(p->vfd);
}


MODULE_EXPORT void
de200c_flush (Driver *drvthis)
// ----------------------------------------------------------------------------
//   Flushes all output to the VFD
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    vfd_update(p->vfd);
}


MODULE_EXPORT void
de200c_string (Driver *drvthis, int x, int y, const char string[])
// ----------------------------------------------------------------------------
//   Prints a string on the lcd display, at position (x,y).
// ----------------------------------------------------------------------------
//   The upper-left is (1,1), and the lower right should be (20,4).
{
    PrivateData *p = drvthis->private_data;
    vfd_printf(p->vfd, X(x), Y(y), "%s", string);
}


MODULE_EXPORT void
de200c_chr (Driver *drvthis, int x, int y, char c)
// ----------------------------------------------------------------------------
//   Prints a character on the lcd display, at position (x,y).
// ----------------------------------------------------------------------------
//   The upper-left is (1,1), and the lower right should be (20,4).
//
{
    PrivateData *p = drvthis->private_data;
    vfd_printf(p->vfd, X(x), Y(y), "%c", c);
}


MODULE_EXPORT void
de200c_set_contrast (Driver *drvthis, int promille)
// ----------------------------------------------------------------------------
//  Sets the contrast
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    vfd_set_brightness(p->vfd, promille / 1000.0);
}


MODULE_EXPORT void
de200c_backlight (Driver *drvthis, int on)
// ----------------------------------------------------------------------------
// Sets the backlight brightness
// ----------------------------------------------------------------------------
{
    //PrivateData *p = drvthis->private_data;
    debug(RPT_DEBUG, "LCD Backlight %s", (on) ? "ON" : "OFF");
}


MODULE_EXPORT void
de200c_hbar(Driver *drvthis, int x, int y, int len, int promille, int options)
// ----------------------------------------------------------------------------
//    Draw an horizontal bar
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    vfd_fill_rectangle(p->vfd,
                       X(x), Y(y) + 1,
                       len * char_width * promille / 1000,
                       char_height - 2);
}


MODULE_EXPORT void
de200c_vbar(Driver *drvthis, int x, int y, int len, int promille, int options)
// ----------------------------------------------------------------------------
//    Draw a vertical bar
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    vfd_fill_rectangle(p->vfd,
                       X(x) + 1, Y(y),
                       char_width - 2,
                       len * char_height * promille / 1000);
}


MODULE_EXPORT void
de200c_heartbeat(Driver *drvthis, int state)
// ----------------------------------------------------------------------------
//    Draw a heartbeat
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    static int timer = 0;
    vfd_printf(p->vfd,
               p->cfg.x + p->cfg.w - char_width, p->cfg.y,
               "%c",
               (timer++ & 7) + 128);
}


MODULE_EXPORT void
de200c_num (Driver *drvthis, int x, int num)
// ----------------------------------------------------------------------------
//   Display a big digit
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    if ((num < 0) || (num > 10))
        return;

    // Initialize big nums if necessary
    if (!p->hasBigNum)
    {
        char buffer[3*18];
        int columns;

#define LOAD_GLYPH(c, cols)                                     \
        memset(buffer, 0, sizeof(buffer));                      \
        columns = sizeof(big##c) / 3;                           \
        memcpy(buffer + 3 * ((cols - columns) / 2),             \
               big##c, sizeof(big##c));                         \
        vfd_define_glyph(p->vfd, c + 160, cols, 3, buffer)
        LOAD_GLYPH(0, 18);
        LOAD_GLYPH(1, 18);
        LOAD_GLYPH(2, 18);
        LOAD_GLYPH(3, 18);
        LOAD_GLYPH(4, 18);
        LOAD_GLYPH(5, 18);
        LOAD_GLYPH(6, 18);
        LOAD_GLYPH(7, 18);
        LOAD_GLYPH(8, 18);
        LOAD_GLYPH(9, 18);
        LOAD_GLYPH(10,6);
        p->hasBigNum = 1;
    }
    vfd_printf(p->vfd, X(x), Y(1), "%c", num+160);
}


MODULE_EXPORT int
de200c_get_free_chars (Driver *drvthis)
// ----------------------------------------------------------------------------
//    Return the number of free characters
// ----------------------------------------------------------------------------
{
    return 30;
}


MODULE_EXPORT void
de200c_set_char (Driver *drvthis, int n, unsigned char *dat)
// ----------------------------------------------------------------------------
//   Define a character
// ----------------------------------------------------------------------------
{
    PrivateData *p = drvthis->private_data;
    unsigned char bytes[6] = { 0 };
    int r, c;
    int mask = 1;
    for (r = 0; r < 8; r++)
    {
        unsigned char src = dat[r];
        for (c = 0; c < 5; c++)
        {
            if (src & 32)
                bytes[c] |= mask;
            src <<= 1;
        }
        mask <<= 1;
    }
    vfd_define_glyph(p->vfd, n, 6, 1, (char *) bytes);
}


MODULE_EXPORT int
de200c_cellwidth (Driver *drvthis)
// ----------------------------------------------------------------------------
//    Return the width of a cell
// ----------------------------------------------------------------------------
{
    return char_width;
}


MODULE_EXPORT int
de200c_cellheight (Driver *drvthis)
// ----------------------------------------------------------------------------
//    Return the height of a cell
// ----------------------------------------------------------------------------
{
    return char_height;
}
