#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>
#include <linux/kd.h>
#include <linux/keyboard.h>
#include <sys/ioctl.h>
#include <errno.h>
#include "getfd.h"
#include "nls.h"
#include "version.h"
#include "vfd.h"

int tmp;	/* for debugging */

int fd;
int oldkbmode;
struct termios old;

/*
 * version 0.81 of showkey would restore kbmode unconditially to XLATE,
 * thus making the console unusable when it was called under X.
 */
static void
get_mode(void) {
        char *m;

	if (ioctl(fd, KDGKBMODE, &oldkbmode)) {
		perror("KDGKBMODE");
		exit(1);
	}
	switch(oldkbmode) {
	  case K_RAW:
	    m = "RAW"; break;
	  case K_XLATE:
	    m = "XLATE"; break;
	  case K_MEDIUMRAW:
	    m = "MEDIUMRAW"; break;
	  case K_UNICODE:
	    m = "UNICODE"; break;
	  default:
	    m = _("?UNKNOWN?"); break;
	}
	printf(_("kb mode was %s\n"), m);
	if (oldkbmode != K_XLATE) {
	    printf(_("[ if you are trying this under X, it might not work\n"
		     "since the X server is also reading /dev/console ]\n"));
	}
	printf("\n");
}

static void
clean_up(void) {
	if (ioctl(fd, KDSKBMODE, oldkbmode)) {
		perror("KDSKBMODE");
		exit(1);
	}
	if (tcsetattr(fd, 0, &old) == -1)
		perror("tcsetattr");
	close(fd);
}

static void
die(int x) {
	printf(_("caught signal %d, cleaning up...\n"), x);
	clean_up();
	exit(1);
}

static void
watch_dog(int x) {
	clean_up();
	exit(0);
}

static void
usage(void) {
	fprintf(stderr, _(
"showkey version %s\n\n"
"usage: showkey [options...]\n"
"\n"
"valid options are:\n"
"\n"
"	-h --help	display this help text\n"
"	-a --ascii	display the decimal/octal/hex values of the keys\n"
"	-s --scancodes	display only the raw scan-codes\n"
"	-k --keycodes	display only the interpreted keycodes (default)\n"
), VERSION);
	exit(1);
}

int
main (int argc, char *argv[]) {
        static char buffer[32];
	const char *short_opts = "haskV";
	const struct option long_opts[] = {
		{ "help",	no_argument, NULL, 'h' },
		{ "ascii",	no_argument, NULL, 'a' },
		{ "scancodes",	no_argument, NULL, 's' },
		{ "keycodes",	no_argument, NULL, 'k' },
		{ "version",	no_argument, NULL, 'V' },
		{ NULL, 0, NULL, 0 }
	};
	int c;
        int count = 0;
	int show_keycodes = 1;
	int print_ascii = 0;
        int x, y, d;
        float bright;
        FILE *fortune = NULL;
        int fscroll = 0;
        char fbytes[160];
        char *fptr;

	struct termios new;
	unsigned char buf[16];
	int i, n;

	set_progname(argv[0]);

	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);

	while ((c = getopt_long(argc, argv,
		short_opts, long_opts, NULL)) != -1) {
		switch (c) {
			case 's':
				show_keycodes = 0;
				break;
			case 'k':
				show_keycodes = 1;
				break;
			case 'a':
				print_ascii = 1;
				break;
			case 'V':
				print_version_and_exit();
			case 'h':
			case '?':
				usage();
		}
	}

	if (optind < argc)
		usage();

	if (print_ascii) {
		/* no mode and signal and timer stuff - just read stdin */
	        fd = 0;

		if (tcgetattr(fd, &old) == -1)
			perror("tcgetattr");
		if (tcgetattr(fd, &new) == -1)
			perror("tcgetattr");

		new.c_lflag &= ~ (ICANON | ISIG);
		new.c_lflag |= (ECHO | ECHOCTL);
		new.c_iflag = 0;
		new.c_cc[VMIN] = 1;
		new.c_cc[VTIME] = 0;

		if (tcsetattr(fd, TCSAFLUSH, &new) == -1)
			perror("tcgetattr");
		printf(_("\nPress any keys - "
		         "Ctrl-D will terminate this program\n\n"));

		while (1) {
			n = read(fd, buf, 1);
			if (n == 1)
				printf(" \t%3d 0%03o 0x%02x\n",
				       buf[0], buf[0], buf[0]);
			if (n != 1 || buf[0] == 04)
				break;
		}

		if (tcsetattr(fd, 0, &old) == -1)
			perror("tcsetattr");
		exit(0);
	}

	fd = getfd();

	/* the program terminates when there is no input for 10 secs */
	signal(SIGALRM, watch_dog);

	/*
		if we receive a signal, we want to exit nicely, in
		order not to leave the keyboard in an unusable mode
	*/
	signal(SIGHUP, die);
	signal(SIGINT, die);
	signal(SIGQUIT, die);
	signal(SIGILL, die);
	signal(SIGTRAP, die);
	signal(SIGABRT, die);
	signal(SIGIOT, die);
	signal(SIGFPE, die);
	signal(SIGKILL, die);
	signal(SIGUSR1, die);
	signal(SIGSEGV, die);
	signal(SIGUSR2, die);
	signal(SIGPIPE, die);
	signal(SIGTERM, die);
#ifdef SIGSTKFLT
	signal(SIGSTKFLT, die);
#endif
	// signal(SIGCHLD, die);
	// signal(SIGCONT, die);
	// signal(SIGSTOP, die);
	// signal(SIGTSTP, die);
	//signal(SIGTTIN, die);
	//signal(SIGTTOU, die);

	get_mode();
	if (tcgetattr(fd, &old) == -1)
		perror("tcgetattr");
	if (tcgetattr(fd, &new) == -1)
		perror("tcgetattr");

	new.c_lflag &= ~ (ICANON | ECHO | ISIG);
	new.c_iflag = 0;
	new.c_cc[VMIN] = sizeof(buf);
	new.c_cc[VTIME] = 1;	/* 0.1 sec intercharacter timeout */

	if (tcsetattr(fd, TCSAFLUSH, &new) == -1)
		perror("tcsetattr");
	if (ioctl(fd, KDSKBMODE, show_keycodes ? K_MEDIUMRAW : K_RAW)) {
		perror("KDSKBMODE");
		exit(1);
	}

	printf(_("Running...\n"));
        VFD_Init();
        VFD_Brightness(.3);
        x = d = 0;
	while (1) {
             char *ptr = buf;
             time_t t = time(NULL);
             float load1, load2, load3;
             int l, l1, l2, l3, s;
             FILE *f = fopen("/proc/loadavg", "r");
             
             // Display oscillating date/time
             d++;
             if (d & 16)
                  x--;
             else
                  x++;
             VFD_SetXY(x, 0);
             VFD_Data(0);
             VFD_Text(ctime(&t));
             
             // Display load averages
             fscanf(f, "%f %f %f", &load1, &load2, &load3);
             fclose(f);
             l1 = load1 * 120;
             l2 = load2 * 120;
             l3 = load3 * 120;
             
             VFD_SetXY(0, 2);
             VFD_Text("Load: ");
             for (l = 0; l < 120; l++)
             {
                  int b = 0;
                  if (l1 >= l) b |= 0x07;
                  if (l2 >= l) b |= 0x1C;
                  if (l3 >= l) b |= 0x70;
                  VFD_Data(b);
             }
             
             // Display current fortune
             for (s = 0; s < 2; s++)
             {
                  if (!fortune)
                  {
                       system ("/usr/games/fortune > /tmp/glop");
                       fortune = fopen("/tmp/glop", "r");
                  }
                  if (fscroll == 0)
                  {
                       c = fgetc(fortune);
                       if (c == EOF)
                       {
                            fclose (fortune);
                            fortune = NULL;
                            c = 0;
                            fscroll = 80;
                       }
                       else
                       {
                            fscroll = 6;
                       }
                       fptr = VFD_Font[c];
                  }
                  memmove(fbytes, fbytes+1, 159);
                  fbytes[159] = *fptr++;
                  fscroll--;
                  VFD_SetXY(0, 1);
             }
             for (l = 0; l < 160; l++)
                  VFD_Data(fbytes[l]);

             // Read hex keys from console
             n = read(fd, buf, sizeof(buf));
             if (n < 0)
             {
                  if (errno != EAGAIN)
                       perror("showkey:read");
                  n = 0;
             }
             count += n;

#define MAX 9
             for (i = 0; i < n; i++)
             {
                  memmove(buffer, buffer+1, MAX);
                  buffer[MAX] = *ptr++;
             }
             
             VFD_SetXY(0,3);
             sprintf(buf, "%d:", count);
             VFD_Text(buf);
             for (i = 0; i < MAX; i++)
             {
                  sprintf(buf, "%02X", (unsigned char) buffer[i]);
                  VFD_Text(buf);
                  VFD_Data(0);
                  VFD_Data(0);
             }
             VFD_Data(-1);

             usleep(30 MS);
	}

	clean_up();
	exit(0);
}
