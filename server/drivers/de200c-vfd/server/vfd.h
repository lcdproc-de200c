#ifndef VFD_H
#define VFD_H
// ****************************************************************************
//  vfd.h                           (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Defines for controlling the VFD800 hardware
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************


// ============================================================================
// 
//    High-level functions
// 
// ============================================================================

extern int VFD_Init();
extern void VFD_EnableScreen(int screen0, int screen1);
extern void VFD_Brightness(float brightness);
extern void VFD_SetX(int X);
extern void VFD_SetY(int Y);
extern void VFD_SetXY(int X, int Y);
extern void VFD_Char(char c);
extern void VFD_Text(char *txt);
extern void VFD_LoadGlyphs(char *glyphtable);
extern int  VFD_IsFont(char *ptr);


// ============================================================================
// 
//    Low-level functions
// 
// ============================================================================

extern void VFD_Command(int command);
extern void VFD_Data(int data);
extern char VFD_MonospacedFont[];
extern char VFD_ProportionalFont[];
extern char *VFD_Font[256];



// ============================================================================
// 
//    Low-level command defines
// 
// ============================================================================

#define DATAPORT        0x378   /* printer port base address */
#define CONTROLPORT     0x37A   /* print port control address */


#define ANDCNTL         0x03
#define ORCNTL          0x01
#define XORCNTL         0x02

#define INIT800A        0x5F    /* initialization code sequence  */
#define INIT800B        0x62
#define INIT800C(n)     0x00+n
#define INIT800D        0xFF
#define CLEARSCREENS    0x5e    /* clear all screens (layers) */
#define LAYER0ON        0x24    /* screen0 both on */
#define LAYER1ON        0x28    /* screen1 both on */
#define LAYERSON        0x2c    /* both screens both on */
#define LAYERSOFF       0x20    /* screens both off */
#define ORON            0x40    /* OR screens */
#define ANDON           0x48    /* AND screens */
#define XORON           0x44    /* XOR screens */
#define SETX            0x64    /* set X position */
#define SETY            0x60    /* set Y position */
#define HSHIFT          0x70    /* set horizontal shift */
#define VSHIFT          0xB0
#define AUTOINCOFF      0x80    /* address auto increment off */
#define AUTOINCX        0x84    // Auto-increment X axis
#define AUTOINCY        0x82    // Auto-increment Y axis
#define AUTOINCXY       0x86    // Auto-increment Y axis
#define SETPOSITION     0xff

#define MS              * 1000  // 1ms = 1000us

#endif // VFD_H
