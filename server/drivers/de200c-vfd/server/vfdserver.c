// ****************************************************************************
//  vfdserver.c                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     A server managing the VFD (Vacuum Fluorescent Display) in the
//     HP Digital Entertainment Center (DEC)
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <string.h>
#include "vfdserver.h"
#include "vfd.h"



// ============================================================================
// 
//   Program globals
// 
// ============================================================================

vfdclient_p clients = NULL;
volatile int clients_writer = 0, clients_reader = 0;
byte display[VFD_SIZE] = { 0 };         // Current display state
byte old_display[VFD_SIZE] = { 0 };     // Previous display state (VFD cache)
long refresh_rate = 40000;              // Refresh rate in microseconds
long reset_rate = 10*60*1000000;        // Reset rate in microseconds
int brightness = -1;                    // The brightness setting
int save_brightness = -1;               // The brightness setting
char *cfg_file = "/etc/vfd.conf";       // Configuration file
int verbose = 0;
pthread_mutex_t clients_lock = PTHREAD_MUTEX_INITIALIZER;

#define dprintf verbose && printf


void Free(char *glyph)
// ----------------------------------------------------------------------------
//   Check if a glyph was allocated dynamically, if so free it
// ----------------------------------------------------------------------------
{
    if (!VFD_IsFont(glyph))
        free(glyph);
}



// ============================================================================
// 
//    Task updating the display
// 
// ============================================================================

int GetChar(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Get next char from buffer
// ----------------------------------------------------------------------------
{
     int rd;
     if (!client->socket)
          return EOF;

     if (client->head >= client->tail)
     {
          // Read some new stuff in the buffer
          rd = read(client->socket,client->command,sizeof(client->command));
          if (rd <= 0)                   // Client broke the connection: die
               return EOF;
          client->head = client->command;
          client->tail = client->command + rd;
     }

     return *client->head++;
}


void UngetChar(vfdclient_p client, char c)
// ----------------------------------------------------------------------------
//   Put the char back in the queue
// ----------------------------------------------------------------------------
{
     if (client->head > client->command)
          *--client->head = c;
}


long GetInt(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Get the largest possible int value
// ----------------------------------------------------------------------------
{
     long result = 0;
     int hadnum = 0;
     int negative = 0;
     int c;
     while (1)
     {
          c = GetChar(client);
          if (c == EOF)
               break;
          if (!hadnum)
          {
               if (isspace(c))
                    continue;
               if (c == '-')
               {
                    negative = !negative;
                    continue;
               }
               if (c == '+')
                    continue;
          }
          if (c < '0' || c > '9')
          {
               UngetChar(client, c);
               break;
          }
          hadnum = 1;
          result = 10 * result + (c - '0');
     }
     if (negative)
          result = -result;
     return result;
}


long GetHex(vfdclient_p client, int maxbytes)
// ----------------------------------------------------------------------------
//   Get an hexadecimal value
// ----------------------------------------------------------------------------
{
     long result = 0;
     int hadnum = 0;
     int c;
     while (hadnum < maxbytes)
     {
          c = GetChar(client);
          if (c == EOF)
               break;
          if (!hadnum && isspace(c))
               continue;
          if (c >= '0' && c <= '9')
               result = 16 * result + (c - '0');
          else if (c >= 'a' && c <= 'f')
               result = 16 * result + (c - 'a' + 10);
          else if (c >= 'A' && c <= 'F')
               result = 16 * result + (c - 'A' + 10);
          else
          {
               UngetChar(client, c);
               break;
          }
          hadnum++;
     }
     return result;
}


inline void Update(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Indicate that new buffer is ready for display
// ----------------------------------------------------------------------------
{
     dprintf("Update(%d:%s)\n", client->socket, client->name);
     memcpy(client->valid, client->display, VFD_SIZE);
     client->writer_index++;
}


inline void UpdateMasks()
// ----------------------------------------------------------------------------
//   Notify that there was a mask update
// ----------------------------------------------------------------------------
{
     dprintf("UpdateMasks\n");
     clients_writer++;
}


void Reply(vfdclient_p client, char *msg)
// ----------------------------------------------------------------------------
//    Send a reply back
// ----------------------------------------------------------------------------
{
     write(client->socket, msg, strlen(msg));
}


inline void SetByte(vfdclient_p client, int x, int y, byte b)
// ----------------------------------------------------------------------------
//   Set a byte in the new array
// ----------------------------------------------------------------------------
{
     if (x >= 0 && x < VFD_COLS && y >= 0 && y < VFD_LINES)
          client->base[y * VFD_COLS + x] = b;
}


inline void SetByteMasked(vfdclient_p client, int x, int y, byte b, byte m)
// ----------------------------------------------------------------------------
//   Set a byte in the new array
// ----------------------------------------------------------------------------
{
     if (x >= 0 && x < VFD_COLS && y >= 0 && y < VFD_LINES)
     {
          byte *bp = &client->base[y * VFD_COLS + x];
          *bp = (*bp & ~m) | (b & m);
     }
}


int DrawGlyph(vfdclient_p client, int c, int x, int y, int inverse)
// ----------------------------------------------------------------------------
//   Draw a given character
// ----------------------------------------------------------------------------
{
     byte *glyph = client->glyphs [(byte) c];
     int width = *glyph++;
     int height = *glyph++;
     int yy = y>>3;
     int cx, cy;
     int shift = y & 7;

     if (shift)
     {
          // Text not aligned with byte boundary
          byte m = 0xFF << shift;
          for (cx = 0; cx < width; cx++)
          {
               for (cy = 0; cy < height; cy++)
               {
                    byte b = *glyph++;
                    if (inverse) b = ~b;
                    SetByteMasked(client, x, yy+cy, b << shift, m);
                    SetByteMasked(client, x, yy+cy+1, b >> (8-shift), ~m);
               }
               x++;
          }
     }
     else
     {
          // Text is aligned with byte boundary
          for (cx = 0; cx < width; cx++)
          {
               for (cy = 0; cy < height; cy++)
               {
                    byte b = *glyph++;
                    if (inverse) b = ~b;
                    SetByte(client, x, yy+cy, b);
               }
               x++;
          }
     }
     return x;
}


void DrawPoint(vfdclient_p client, int x, int y, int inverse)
// ----------------------------------------------------------------------------
//   Draw a single point
// ----------------------------------------------------------------------------
{
     int yy = y>>3;
     byte mask = 1 << (y & 7);
     if (inverse)
          SetByteMasked(client, x, yy, 0, mask);
     else
          SetByteMasked(client, x, yy, -1, mask);
}


void DrawRectangle(vfdclient_p client,
                   int x, int y, int w, int h,
                   int inverse, int fill)
// ----------------------------------------------------------------------------
//   Draw a rectangle with the given coordinates
// ----------------------------------------------------------------------------
{
     int hiy = y+h-1;
     int loy = y;
     int hh = h;
     if (hiy >= VFD_PLINES)
          hiy = VFD_PLINES-1;
     if (hiy < 0)
          hiy = 0;
     if (loy >= VFD_PLINES)
          loy = VFD_PLINES-1;
     if (loy < 0)
          loy = 0;
     hh = hiy - loy + 1;
          
     if (w > 0 && hh > 0)
     {
          long fillmask = ((hh < 32 ? (1L << hh) : 0) - 1L) << loy;
          byte h1, h2, h3, h4;
          
          byte f1, f2, f3, f4;
          int rx, ry;
          
          f1 = fillmask;
          f2 = fillmask >> 8;
          f3 = fillmask >> 16;
          f4 = fillmask >> 24;
          
          if (!fill)
          {
               long hollowmask = ((1L<<(hiy)) | (1L<<loy));
               h1 = hollowmask;
               h2 = hollowmask >> 8;
               h3 = hollowmask >> 16;
               h4 = hollowmask >> 24;
          }
          else
          {
               h1 = f1;
               h2 = f2;
               h3 = f3;
               h4 = f4;
          }
          
          if (inverse)
          {
               for (rx = x; rx < x + w; rx++)
               {
                    SetByteMasked(client, rx, 0, 0, h1);
                    SetByteMasked(client, rx, 1, 0, h2);
                    SetByteMasked(client, rx, 2, 0, h3);
                    SetByteMasked(client, rx, 3, 0, h4);
               }
          }
          else
          {
               for (rx = x; rx < x + w; rx++)
               {
                    SetByteMasked(client, rx, 0, -1, h1);
                    SetByteMasked(client, rx, 1, -1, h2);
                    SetByteMasked(client, rx, 2, -1, h3);
                    SetByteMasked(client, rx, 3, -1, h4);
               }
          }
          
          if (!fill)
          {
               rx = x + w - 1;
               if (inverse)
               {
                    SetByteMasked(client, x, 0, 0, f1);
                    SetByteMasked(client, x, 1, 0, f2);
                    SetByteMasked(client, x, 2, 0, f3);
                    SetByteMasked(client, x, 3, 0, f4);
                    
                    SetByteMasked(client, rx, 0, 0, f1);
                    SetByteMasked(client, rx, 1, 0, f2);
                    SetByteMasked(client, rx, 2, 0, f3);
                    SetByteMasked(client, rx, 3, 0, f4);
               }
               else
               {
                    SetByteMasked(client, x, 0, -1, f1);
                    SetByteMasked(client, x, 1, -1, f2);
                    SetByteMasked(client, x, 2, -1, f3);
                    SetByteMasked(client, x, 3, -1, f4);
                    
                    SetByteMasked(client, rx, 0, -1, f1);
                    SetByteMasked(client, rx, 1, -1, f2);
                    SetByteMasked(client, rx, 2, -1, f3);
                    SetByteMasked(client, rx, 3, -1, f4);
               }
          }
     }
}


void DrawLine(vfdclient_p client,
              int x, int y, int w, int h,
              int inverse)
// ----------------------------------------------------------------------------
//   Draw a line
// ----------------------------------------------------------------------------
{
     int tx = x + w;
     int ty = y + h;
     int sx = w >= 0 ? 1 : -1;
     int sy = h >= 0 ? 1 : -1;
     int ax = h >= 0 ? h : -h;
     int ay = w >= 0 ? w : -w;
     int delta = ax > ay ? -1 : 0;

     while (x != tx || y != ty)
     {
          DrawPoint(client, x, y, inverse);
          if (delta >= 0)
          {
               x += sx;
               delta -= ax;
          }
          if (delta < 0)
          {
               y += sy;
               delta += ay;
          }
     }
}


void DrawEllipse(vfdclient_p client,
                 int x, int y, int w, int h,
                 int inverse, int fill)
// ----------------------------------------------------------------------------
//   Draw an ellipse
// ----------------------------------------------------------------------------
{
     if (w > 0 && h > 0)
     {
          long delta = 0;
          long ax = h * h;
          long ay = w * w;
          int ex = -w, ey = 0;
          int cx = x + w/2, cy = y + h/2;
          int grow= 1;

          while (ex != 0)
          {
               if (fill)
               {
                    DrawRectangle(client,cx+ex/2,cy-ey/2,1,ey-1,inverse,1);
                    DrawRectangle(client,cx-ex/2,cy-ey/2,1,ey-1,inverse,1);
               }
               else
               {
                    DrawPoint(client, cx + ex/2, cy - ey/2, inverse);
                    DrawPoint(client, cx + ex/2, cy + ey/2 -1, inverse);
                    DrawPoint(client, cx - ex/2, cy - ey/2, inverse);
                    DrawPoint(client, cx - ex/2, cy + ey/2 -1, inverse);
               }
               if (delta >= 0)
               {
                    delta -= ay * ey;
                    ey += 1;
               }
               if (delta < 0)
               {
                    ex += 1;
                    delta -= ax * ex;
               }
          }
     }
}


void InvertRectangle(vfdclient_p client,
                     int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Invert the rectangle at the given coordinates
// ----------------------------------------------------------------------------
{
     int hiy = y + h - 1;
     int loy = y;
     int hh = h;
     int lox = x;
     int hix = x + w - 1;
     int ww = w;

     if (hiy >= VFD_PLINES)
          hiy = VFD_PLINES-1;
     if (hiy < 0)
          hiy = 0;
     if (loy >= VFD_PLINES)
          loy = VFD_PLINES-1;
     if (loy < 0)
          loy = 0;
     hh = hiy - loy - 1;
          
     if (hix >= VFD_COLS)
          hix = VFD_COLS-1;
     if (hix < 0)
          hix = 0;
     if (lox >= VFD_COLS)
          lox = VFD_COLS-1;
     if (lox < 0)
          lox = 0;
     ww = hix - lox - 1;

     if (ww > 0 && hh > 0)
     {
          long fillmask = ((hh < 32 ? (1L << hh) : 0) - 1L) << loy;
          byte xorval;
          int rx, ry;
          
          for (ry = 0; ry < VFD_LINES; ry++)
          {
               byte *bp = &client->base[lox + ry * VFD_COLS];
               xorval = fillmask >> (8 * ry);
               
               for (rx = lox; rx < hix; rx++)
                    *bp++ ^= xorval;
          }
     }
}


void DefineGlyph(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Change the definition of a particular glyph
// ----------------------------------------------------------------------------
{
     byte c = GetInt(client);
     int w = GetInt(client);
     int h = GetInt(client);
     if (w > 0 && w <= VFD_COLS &&
         h > 0 && h <= VFD_LINES)
     {
          byte *ptr;
          int n = h * w;
          Free(client->glyphs[c]);
          ptr = malloc(n + 2);
          client->glyphs[c] = ptr;
          *ptr++ = w;
          *ptr++ = h;
          while (n--)
               *ptr++ = GetHex(client, 2);
     }
     else
     {
          Reply(client, "?Glyph\n");
     }
}


void SelectFont(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Load an entire font
// ----------------------------------------------------------------------------
{
    int font = GetInt(client);
    char *glyphs = VFD_MonospacedFont;
    int c;
    
    switch (font)
    {
    case 0:
        // Monospace font
        glyphs = VFD_MonospacedFont;
        break;
    case 1:
        glyphs = VFD_ProportionalFont;
        break;
    }

    for (c = 0; c < 256; c++)
    {
        char *base = glyphs;
        char width = *glyphs++;
        char height = *glyphs++;
        glyphs += width * height;

        Free(client->glyphs[c]);
        client->glyphs[c] = base;
    }
}


void TextToGlyph(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Convert the incoming text to a glyph
// ----------------------------------------------------------------------------
{
     int w = 0, h = 0;
     char text[256];
     int i, n = 0, hh, c;
     byte *ptr, *new_glyph;
     int cx, cy;

     // Glyph number
     c = GetInt(client);
     if (GetChar(client) != ':')
     {
          Reply(client, "?TextToGlyph\n");
          return;
     }

     // Get the text in a buffer
     while ((c = GetChar(client)) &&
            c != EOF && c != '\r' && c != '\n')
          if (n + 1 < sizeof(text))
               text[n++] = c;
     text[n] = 0;

     // Compute text extent
     for (i = 0; i < n; i++)
     {
          byte *glyph = client->glyphs[(byte) text[i]];
          w += *glyph++;
          hh = *glyph++;
          if (hh > h)
               h = hh;
     }

     // Allocate new glyph
     Free(client->glyphs[(byte) c]);
     ptr = malloc(h*w + 2);
     new_glyph = ptr;
     *ptr++ = w;
     *ptr++ = h;

     // Draw into newly allocated glyph
     for (i = 0; i < n; i++)
     {
          byte *glyph = client->glyphs[(byte) text[i]];
          int gw = *glyph++;
          int gh = *glyph++;
          
          for (cx = 0; cx < gw; cx++)
          {
               for (cy = 0; cy < h; cy++)
               {
                    // Individual glyphs in text may not be as high
                    if (cy < gh)
                         *ptr++ = *glyph++;
                    else
                         *ptr++ = 0;
               }
          }
     }

     // Update at end, just in case someone would be silly enough to use
     // glyph being updated in the draw string...
     client->glyphs[(byte) c] = new_glyph;

     // Send dimensions back
     sprintf(text, "W%d H%d\n", w, h * 8);
     Reply(client, text);
}


void Configure(vfdclient_p client)
// ----------------------------------------------------------------------------
//   Try to read the configuration file for the given name
// ----------------------------------------------------------------------------
{
     int x = 0, y = 0, w = 0, h = 0, prio = 0;
     char line[256];
     char *ptr;
     FILE *f = fopen(cfg_file, "r");
     if (f)
     {
          bzero(line, sizeof(line));
          while (!feof(f))
          {
               if (!fgets(line, sizeof(line)-1, f))
                    break;
               if (line[0] == '#')
                    continue;
               ptr = strchr(line, ':');
               if (ptr)
               {
                    *ptr = 0;
                    if (!strcmp(line, client->name))
                    {
                         if (sscanf(ptr+1,"%i %i %i %i %i",
                                    &x,&y,&w,&h,&prio) < 5)
                              fprintf(stderr,
                                      "%s: Incorrect configuration for '%s'\n",
                                      cfg_file, line);
                         if (x < 0 || x >= VFD_COLS)
                         {
                              fprintf(stderr,
                                      "%s: Incorrect X=%d for '%s'\n",
                                      cfg_file, x, line);
                              x = 0;
                         }
                         if (y < 0 || y >= VFD_PLINES)
                         {
                              fprintf(stderr,
                                      "%s: Incorrect Y=%d for '%s'\n",
                                      cfg_file, y, line);
                              y = 0;
                         }
                         if (w < 0 || x+w > VFD_COLS)
                         {
                              fprintf(stderr,
                                      "%s: Incorrect W=%d for '%s'\n",
                                      cfg_file, w, line);
                              w = 0;
                         }
                         if (h < 0 || y+h > VFD_PLINES)
                         {
                              fprintf(stderr,
                                      "%s: Incorrect H=%d for '%s'\n",
                                      cfg_file, h, line);
                              h = 0;
                         }
                         break;
                    }
               }
          }
          fclose(f);
     }

     dprintf("Configure(%d:%s) X=%d Y=%d W=%d H=%d P=%d\n",
             client->socket, client->name, x, y, w, h, prio);

     // Select the mask from the given region
     ptr = client->base;
     client->base = client->shape;
     bzero(client->base, VFD_SIZE);
     DrawRectangle(client, x, y, w, h, 0, 1); // Fill mask
     client->base = ptr;
     client->priority = prio;
     UpdateMasks();

     // Reply what we found
     sprintf(line, "X%d Y%d W%d H%d P%d\n", x, y, w, h, prio);
     Reply(client, line);
}


void *ClientTask(void *client_ptr)
// ----------------------------------------------------------------------------
//   Process incoming messages for the given client
// ----------------------------------------------------------------------------
{
     vfdclient_p client = (vfdclient_p) client_ptr;
     int running = 1;
     int c;
     int x = 0, y = 0, w = VFD_COLS, h = VFD_LINES * 8;
     int inverse = 0, fill = 1;
     int n;
     char reply[32];


     dprintf("ClientTask(%d:%s)\n", client->socket, client->name);

     // By default, client takes over the whole screen
     memset(client->shape, -1, VFD_SIZE);
     bzero(client->display, VFD_SIZE);
     Update(client);
     UpdateMasks();

     Reply(client, "Ready\n");

     while (running)
     {
          switch(c = GetChar(client))
          {
               // Configuration
          case 'N':             // Name
               n = 0;
               while ((c = GetChar(client)) &&
                      c != EOF && c != '\r' && c != '\n')
                    if (n + 1 < sizeof(client->name))
                         client->name[n++] = c;
               client->name[n] = 0;
               Configure(client);
               break;
          case 'Z':
               client->priority = GetInt(client);
               UpdateMasks();
               break;
          case 'G':             // Define glyph
               DefineGlyph(client);
               break;
          case 'I':             // Text to glyph conversion
               TextToGlyph(client);
               break;
          case 'J':
               SelectFont(client);
               break;
          case 'B':             // Brightness
               n = GetInt(client);
               if (n >= 0 && n <= 255)
                    brightness = n;
               break;

               // Coordinates
          case 'X':             // Set X
               x = GetInt(client);
               break;
          case 'Y':             // Set Y
               y = GetInt(client);
               break;
          case 'W':             // Set W
               w = GetInt(client);
               break;
          case 'H':             // Set H
               h = GetInt(client);
               break;
          case '+':             // Set normal video
               inverse = 0;
               break;
          case '-':             // Set inverse video
               inverse = 1;
               break;
          case 'M':             // Select the mask bits
               client->base = client->shape;
               break;
          case 'D':             // Select the display bits
               client->base = client->display;
               UpdateMasks();
               break;
          case 'F':             // Fill
               fill = 1;
               break;
          case 'O':             // Hollow
               fill = 0;
               break;

               // Drawing primitives
          case 'C':             // Clear screen
               if (inverse)
                    memset(client->base, -1, VFD_SIZE);
               else
                    bzero(client->base, VFD_SIZE);
               break;
          case 'T':             // Display text
               while ((c = GetChar(client)) &&
                      c != EOF && c != '\n' && c != '\r')
                    x = DrawGlyph(client, c, x, y, inverse);
               break;
          case 'R':             // Draw a rectangle
               DrawRectangle(client, x, y, w, h, inverse, fill);
               break;
          case 'P':             // Draw a point
               DrawPoint(client, x, y, inverse);
               break;
          case 'L':             // Draw a line
               DrawLine(client, x, y, w, h, inverse);
               x += w;
               y += h;
               break;
          case 'E':             // Draw an ellipse
               DrawEllipse(client, x, y, w, h, inverse, fill);
               break;
          case '*':
               InvertRectangle(client, x, y, w, h);
               break;
          case 'A':             // Compute text area
               w = 0;
               h = 0;
               while ((c = GetChar(client)) &&
                      c != EOF && c != '\n' && c != '\r')
               {
                    byte *ptr = client->glyphs[(byte) c];
                    w += *ptr++;
                    n = *ptr++;
                    if (n > h)
                         h = n;
               }
               sprintf(reply, "W%d H%d\n", w, h * 8);
               Reply(client, reply);
               break;

          case 'U':
               Update(client);
               break;

          case 'V':
               Reply(client, "VFD800 Server Version 1\n");
               break;

          case 'Q':
               dprintf("Quit(%d:%s)\n", client->socket, client->name);
               Reply(client, "Bye!\n");
               running = 0;
               break;
          case EOF:
               dprintf("Disconnect(%d:%s)\n", client->socket, client->name);
               running = 0;
               break;

          default:
               if (c > ' ')
               {
                    sprintf(reply, "?Command %02X (%c)\n", c, c);
                    Reply(client, reply);
               }
               break;
          }
     } // While(1)
     dprintf("ClientTaskExit(%d:%s)\n", client->socket, client->name);
     close(client->socket);
     client->socket = 0;
     UpdateMasks();
}


void RecomputeMasks()
// ----------------------------------------------------------------------------
//   Some global change to the list: recompute the masks for each client
// ----------------------------------------------------------------------------
{
     byte mask[VFD_SIZE];
     vfdclient_p client, previous = NULL, next = NULL;
     int b;
     long *cur_mask = (long *) mask;
     long *shape, *dst_mask;
     long *disp = (long *) display;
     long *cldisp;
     int unsorted;

     pthread_mutex_lock(&clients_lock);

     dprintf("RecomputeMasks\n");
     do
     {
          clients_reader = clients_writer;           // Check-point
          memset(mask, -1, VFD_SIZE);                // Default mask
          bzero(display, VFD_SIZE);
          unsorted = 0;

	  previous = NULL;
          for (client = clients; client; client = next)
          {
               next = client->next;

               dprintf("Mask(%d:%s) P%d\n",
                       client->socket, client->name, client->priority);

               // Check if some clients are gone
               if (client->socket == 0)
               {
                    dprintf("Kill(%d:%s)\n", client->socket, client->name);
                    if (previous)
                         previous->next = next;
                    else
                         clients = next;
                    free(client);
                    continue;
               }
               
               // If unsorted, sort and restart
               if (next && client->priority < next->priority)
               {
                    dprintf("Unsorted(%d:%s)=%d < %d\n",
                            client->socket, client->name,
                            client->priority, next->priority);
                    if (previous)
                         previous->next = next;
                    else
                         clients = next;
                    client->next = next->next;
                    next->next = client;
                    unsorted = 1;
                    break;
               }
               
               // Otherwise, recompute client mask and display state
               shape = (long *) client->shape;
               dst_mask = (long *) client->mask;
               cldisp = (long *) client->valid;
               for (b = 0; b < VFD_SIZE / sizeof(long); b++)
               {
                    dst_mask[b] = cur_mask[b] & shape[b];
                    cur_mask[b] &= ~shape[b];
                    disp[b] = ((disp[b] & ~dst_mask[b]) |
                               (cldisp[b] & dst_mask[b]));
               }
               previous = client;
          }
     } while (unsorted);
     if (verbose)
     {
       dprintf("RecomputeMasks:\n");
       for (client = clients; client; client = client->next)
	 dprintf("  %s(%d P%d)\n",
		 client->name, client->socket, client->priority);
     }
     pthread_mutex_unlock(&clients_lock);


}


void *DisplayTask(void *unused)
// ----------------------------------------------------------------------------
//    Recompute current display state
// ----------------------------------------------------------------------------
{
     vfdclient_p client;
     int b;
     long *mask, *disp;
     long before_reset = reset_rate;

     dprintf("DisplayTask\n");
     while (1)
     {
          byte *oldb = old_display;
          byte *newb = display;
          long *oldd = (long *) oldb;
          long *newd = (long *) newb;
          int l, c;
          int lastl, lastc, lastb;
          int updated = 0;

          // Check if there are global changes to the client list
          if (clients_writer != clients_reader)
               RecomputeMasks();
          if (brightness >= 0)
          {
               VFD_Brightness(brightness / 255.0);
               save_brightness = brightness;
               brightness = -1;
          }

          // Compute the new display contents
	  pthread_mutex_lock(&clients_lock);
          for (client = clients; client; client = client->next)
          {
               // Check if any particular client updated
               if (client->reader_index != client->writer_index)
               {
                    dprintf("Refresh(%d:%s)\n", client->socket, client->name);

                    // Check-point
                    client->reader_index = client->writer_index;

                    // Mask and display for that client
                    mask = (long *) client->mask;
                    disp = (long *) client->valid;

                    // Make update to new data
                    for (b = 0; b < VFD_SIZE / sizeof(long); b++)
                         newd[b] = (newd[b] & ~mask[b]) | (disp[b] & mask[b]);
                    updated = 1;
               }
          }
	  pthread_mutex_unlock(&clients_lock);
          
          // Check what we need to update, identifying deltas
          // (we assume that the parallel port is much slower than memory)
          if (updated)
          {
               lastl = lastc = -1;
               VFD_Command(AUTOINCX);
               b = lastb = 0;
#if 1
               for (l = 0; l < VFD_LINES; l++)
               {
                    for (c = 0; c < VFD_COLS; c++)
                    {
                         // Check for changed bytes
                         if (newb[b] != oldb[b])
                         {
                              // If changed, check how to move there...
                              if (lastl != l)
                              {
                                   VFD_SetXY(c,l);
                              }
                              else if (lastc != c)
                              {
                                   if (c - lastc > 2)
                                   {
                                        VFD_SetX(c);
                                   }
                                   else while (++lastc < c)
                                   {
                                        VFD_Data(newb[++lastb]);
                                   }
                              }
                              VFD_Data(newb[b]);
                              lastb = b;
                              lastl = l;
                              lastc = c;
                              oldb[b] = newb[b];
                         }
                         b++;
                    }
               }
#else
               for (l = 0; l < VFD_LINES; l++)
               {
                    VFD_SetXY(0, l);
                    for (c = 0; c < VFD_COLS; c++)
                    {
                         // Check for changed bytes
                         VFD_Data(newb[b]);
                         b++;
                    }
               }
#endif
          }

          // Wait a little bit (about 10ms typical) and start over
          usleep(refresh_rate);

          // Check if we need to reset display for safety
          before_reset -= refresh_rate;
          if (before_reset < 0)
          {
              before_reset = reset_rate;
              VFD_Init();
              if (save_brightness >= 0)
                  VFD_Brightness(save_brightness / 255.0);
              memset(oldb, 0, VFD_SIZE);
          }
     }
}



// ============================================================================
// 
//    Main entry point and option processing
// 
// ============================================================================

int Help2(char *msg1, char *msg2)
// ----------------------------------------------------------------------------
//   Show help
// ----------------------------------------------------------------------------
{
     if (msg1)
          printf("*** %s%s\n", msg1, msg2);
     printf("Usage: vfdserver [options]\n");
#define OPTION(name, usage, code)   printf("%15s: %s\n", #name, usage);
#define FLAG(name, usage, code)     printf("%15s: %s\n", "[no]" #name, usage);
#include "options.tbl"
     if (msg1)
          printf("*** %s%s\n", msg1, msg2);
     exit(0);
     return 0;
}


#define Help1(msg)      Help2(msg, "")
#define Help()          Help2(NULL, "")


long Default(char *name, long value)
// ----------------------------------------------------------------------------
//   Get a default value, unless taken from an environment
// ----------------------------------------------------------------------------
{
     if (getenv(name))
          sscanf(getenv(name), "%li", &value);
     return value;
}


void Error(char *msg)
// ----------------------------------------------------------------------------
//   Report problems
// ----------------------------------------------------------------------------
{
     perror(msg);
     exit (1);
}


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//    Parse command-line parameters
// ----------------------------------------------------------------------------
{
     int port = Default("VFDPORT", 12568);
     int sockfd, newsockfd, clilen;
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     int arg;
     pthread_t thread;

     // Parse options
     for (arg = 1; arg < argc; arg++)
     {
          char *opt = argv[arg];
          int boolval = 0;

          // Skip '-' in front of options.
          while (opt[0] == '-')
               opt++;

#define OPTION(name, usage, code)               \
          if (!strcmp(opt, #name)) { code; } else
#define FLAG(name, usage, code)                                 \
         if (!strcmp(opt, #name)) { boolval = 1; code; } else   \
         if (!strcmp(opt, "no" #name)) { boolval = 0; code; } else
#define BOOL    boolval
#define INT     ((++arg < argc) ? atol(argv[arg]) : Help2("Missing int value", argv[arg]))
#define STRING  ((++arg < argc) ? argv[arg] : (Help2("Missing string", argv[arg]),""))
#include "options.tbl"

          Help2 ("Unknown option:", argv[arg]);
     }

     // Ignore signals which are mostly harmess...
     signal(SIGHUP, SIG_IGN);
     signal(SIGPIPE, SIG_IGN);
     signal(SIGTERM, SIG_IGN);
     signal(SIGCHLD, SIG_IGN);
     signal(SIGCONT, SIG_IGN);
     signal(SIGSTOP, SIG_IGN);
     signal(SIGTSTP, SIG_IGN);
     signal(SIGTTIN, SIG_IGN);
     signal(SIGTTOU, SIG_IGN);

     // Initialize the VFD
     VFD_Init();

     // Initialize the thread mutex
     pthread_mutex_init (&clients_lock, NULL);

     // Open socket and listen to it
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
          Error("Can't open socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(port);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
          Error("Can't bind to socket");
     listen(sockfd,5);          // 5 backlog seems OK for this kind of app?

     // Spawn the display update thread
     if (pthread_create(&thread, NULL, DisplayTask, NULL) < 0)
          Error("Can't spawn display thread");
     
     while (1)
     {
          vfdclient_p client = NULL;

          clilen = sizeof(cli_addr);
          newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
          if (newsockfd < 0) 
               Error("Could not accept socket message");

          // Allocate a client for it
          client = (vfdclient_p) malloc(sizeof(*client));
          if (!client)
               Error("Out of memory (client allocation)");
          client->writer_index = client->reader_index = 0;
          client->socket = newsockfd;
          client->head = client->tail = NULL;
          client->base = client->display;
          client->priority = 0;
          memcpy(client->glyphs, VFD_Font, sizeof(client->glyphs));
          strcpy(client->name, "default");

          // Link in clients list
	  pthread_mutex_lock(&clients_lock);
          client->next = clients;
          clients = client;
	  pthread_mutex_unlock(&clients_lock);
          UpdateMasks();

          // Spawn a thread to deal with this client
          dprintf("Connect(%d:%s)\n", client->socket, client->name);
          if (pthread_create(&thread, NULL, ClientTask, client) < 0)
               Error("Can't create new thread");
     }
     return 0; 
}

