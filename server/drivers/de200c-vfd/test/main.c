// ****************************************************************************
//  main.c                          (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Main entry point of the VFD800 control program
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#include "vfd.h"
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <string.h>


int main(int argc, char **argv)
// ----------------------------------------------------------------------------
//    Entry point for the program
// ----------------------------------------------------------------------------
{
    int x, y, d;
    float bright;
    FILE *fortune = NULL;
    int fscroll = 0;
    char fbytes[160];
    char *fptr;
    int c;


    VFD_Init();

    for (d = 0; d <= 256; d++)
    {
         VFD_Brightness(.5 + .5 * d / 256.0);
         for (y = 0; y < 4; y++)
         {
              VFD_SetXY(0, y);
              VFD_Command(AUTOINCX);
              for (x = 0; x < 160; x++)
                   VFD_Data(d);
         }
    }

    for (bright = 1.0; bright >= 0.0; bright -= 0.1)
    {
         VFD_Brightness(bright);
         usleep(10 MS);
    }

    VFD_SetXY(0, 1);
    VFD_Text("abcdefghijklmnopqrstuvwxyz");

    VFD_SetXY(0,3);
    VFD_Text("J'aime Christine a la folie!");

    for (bright = 0.0; bright <= 1.0; bright += 0.1)
    {
         VFD_Brightness(bright);
         usleep(10 MS);
    }
    usleep(500 MS);

    VFD_Brightness(.3);
    x = d = 0;
    while (1)
    {
         time_t t = time(NULL);
         float load1, load2, load3;
         int l, l1, l2, l3, s;
         FILE *f = fopen("/proc/loadavg", "r");

         // Display oscillating date/time
         d++;
         if (d & 16)
              x--;
         else
              x++;
         VFD_SetXY(x, 0);
         VFD_Data(0);
         VFD_Text(ctime(&t));

         // Display load averages
         fscanf(f, "%f %f %f", &load1, &load2, &load3);
         fclose(f);
         l1 = load1 * 120;
         l2 = load2 * 120;
         l3 = load3 * 120;

         VFD_SetXY(0, 2);
         VFD_Text("Load: ");
         for (l = 0; l < 120; l++)
         {
              int b = 0;
              if (l1 >= l) b |= 0x07;
              if (l2 >= l) b |= 0x1C;
              if (l3 >= l) b |= 0x70;
              VFD_Data(b);
         }

         // Display current fortune
         for (s = 0; s < 2; s++)
         {
              if (!fortune)
              {
                   system ("/usr/games/fortune > /tmp/glop");
                   fortune = fopen("/tmp/glop", "r");
              }
              if (fscroll == 0)
              {
                   c = fgetc(fortune);
                   if (c == EOF)
                   {
                        fclose (fortune);
                        fortune = NULL;
                        c = 0;
                        fscroll = 80;
                   }
                   else
                   {
                        fscroll = 6;
                   }
                   fptr = VFD_Font[c];
              }
              memmove(fbytes, fbytes+1, 159);
              fbytes[159] = *fptr++;
              fscroll--;
              VFD_SetXY(0, 1);
         }
         for (l = 0; l < 160; l++)
              VFD_Data(fbytes[l]);

         usleep(30 MS);
    }



    return 0;
}



int showchars()
// ----------------------------------------------------------------------------
//   Show the character map
// ----------------------------------------------------------------------------
{
     int x, y, d;
     for (x = 0; x < 256; x++)
     {
          printf("{ ");
          for (y = 0; y < 6; y++)
          {
               int byte = VFD_Font[x][y];
               unsigned char z = byte;
               if (0) for (d = 0; d < 8; d++)
               {
                    z <<= 1;
                    if (byte & 1)
                         z |= 1;
                    byte >>= 1;
               }
               printf("0x%02X%s ", z, y < 5 ? "," : "");
          }
          printf("}%s /* 0x%02X %c */\n",
                 x < 255 ? "," : " ", x, isprint(x) ? x : ' ');
     }
}
