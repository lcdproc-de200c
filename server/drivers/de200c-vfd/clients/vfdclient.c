// ****************************************************************************
//  vfdclient.c                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Client talking to the VFD server 
//
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>
#include <stdarg.h>
#include <math.h>


void error(char *msg)
{
    perror(msg);
    exit(0);
}


int sockprintf(int fd, const char *fmt, ...)
// ----------------------------------------------------------------------------
//   Send a formatted message onto a socket
// ----------------------------------------------------------------------------
{
     char buffer[256];
     va_list va;
     va_start(va, fmt);
     bzero(buffer, sizeof(buffer));
     vsnprintf(buffer, sizeof(buffer)-1, fmt, va);
     va_end(va);
     write(fd, buffer, strlen(buffer));
}


void RunClock(int fd, char *config)
// ----------------------------------------------------------------------------
//   Loop, displaying the clock
// ----------------------------------------------------------------------------
{
     char line[256];
     int x, y, w, h, prio;
     int first = 1;

     do
     {
          if (!first)
               sleep(1);
          first = 0;
          sockprintf(fd, "N%s\n", config);
          read(fd, line, sizeof(line));
          sscanf(line, "X%d Y%d W%d H%d P%d", &x, &y, &w, &h, &prio);
     } while (w < 10 || h < 10);

     // Set mask to cover the clock itself
     sockprintf(fd, "MCX%d Y%d W%d H%d ED", x, y, w, h);

     // Display inner circle
     w -= 2;
     h -= 2;
     x += 1;
     y += 1;
     sockprintf(fd, "X%d Y%d W%d H%d OE", x, y, w, h);

     while (1)
     {
          time_t t = time(NULL);
          struct tm *tm = localtime(&t);

          int x0 = x + w/2;
          int y0 = y + h/2;

          int wsec = w / 2.1 * sin(tm->tm_sec / 60.0 * M_PI * 2);
          int hsec = -h / 2.1 * cos(tm->tm_sec / 60.0 * M_PI * 2);

          int wmin = w / 2.5 * sin(tm->tm_min / 60.0 * M_PI * 2);
          int hmin = -h / 2.5 * cos(tm->tm_min / 60.0 * M_PI * 2);

          int whr = w / 3.5 * sin(tm->tm_hour / 12.0 * M_PI * 2);
          int hhr = -h / 3.5 * cos(tm->tm_hour / 12.0 * M_PI * 2);

          // Draw the hands
          sockprintf(fd, "+X%d Y%d W%d H%d L", x0, y0, wmin, hmin);
          sockprintf(fd, "X%d Y%d W%d H%d L", x0, y0, whr, hhr);
          sockprintf(fd, "X%d Y%d W%d H%d L U", x0, y0, wsec, hsec);

          // Sleep
          usleep(10000);

          // Erase the hands
          sockprintf(fd, "-X%d Y%d W%d H%d L", x0, y0, wmin, hmin);
          sockprintf(fd, "X%d Y%d W%d H%d L", x0, y0, whr, hhr);
          sockprintf(fd, "X%d Y%d W%d H%d L", x0, y0, wsec, hsec);
     }
}


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port config\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,&serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    RunClock(sockfd, argv[3]);
    return 0;
}

