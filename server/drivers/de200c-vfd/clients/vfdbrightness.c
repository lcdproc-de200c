// ****************************************************************************
//  vfdbrightness.c                 (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Select the brightness for the VFD
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#define VFDCLIENT               "vfbrightness"
#define VFDOPTIONS              "options.tbl"
#define VFDCONFIG               "brightness"

#include "vfdclient.h"


int main (int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Change VFD brightness
// ----------------------------------------------------------------------------
{
     int vfd;

     if (argc < 2)
          Help();

     vfd = vfd_open(server, port);
     vfd_set_brightness(vfd, atoi(argv[1]) / 100.0);
}
