// ****************************************************************************
//  vfddisplay.c                      (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Pops up an alert on the display
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#define VFDCLIENT               "vfddisplay"
#define VFDOPTIONS              "vfddisplay.tbl"
#define VFDCONFIG               "alert"
long refresh_rate = 100000;
int alignment = 0;

#include "vfdclient.h"


void Run(int vfd, char *config)
// ----------------------------------------------------------------------------
//   Loop, displaying the alert message
// ----------------------------------------------------------------------------
{
     char line[256];
     vfd_configuration cfg;
     int x, y, w, h;
     int color = 1;
     vfd_glyph_extent extent;

     // Wait for the configuration to actually display something
     cfg = vfd_configure(vfd, config);
     x = cfg.x; y = cfg.y; w = cfg.w; h = cfg.h;

     bzero(line, sizeof(line));
     while (!feof(stdin))
     {
          int cx, cy;

          if (!fgets(line, sizeof(line)-1, stdin))
               break;

          extent = vfd_printf_area(vfd, "%s", line);
          if (alignment == 0)
          {
               if (extent.w < w)
                    cx = x + (w - extent.w)/2;
               else
                    cx = x;
          }
          else if (alignment > 0)
          {
               cx = x + (w - extent.w);
          }
          else if (alignment < 0)
          {
               cx = x;
          }
          if (extent.h < h)
               cy = y + (h - extent.h)/2;
          else
               cy = y;

          vfd_clear(vfd);
          vfd_printf(vfd, cx, cy, "%s", line);
          vfd_update(vfd);

          // Sleep
          usleep(refresh_rate);
     }
}


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
    int vfd;
    vfd_options(argc, argv);
    vfd = vfd_open(server, port);
    Run(vfd, cfg);
    return 0;
}

