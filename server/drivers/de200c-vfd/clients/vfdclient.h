#ifndef VFDCLIENT_H
#define VFDCLIENT_H
// ****************************************************************************
//  vfdclient.h                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Client-side API for talking to the Vaccuum Fluorescent Display (VFD)
//     server
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************
//
// The protocol is simple enough that everything is implemented in a .h file

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

#ifndef VFDCLIENT
#error VFDCLIENT (client name) must be set
#endif
#ifndef VFDOPTIONS
#error VFDOPTIONS (option file) must be set
#endif
#ifndef VFDCONFIG
#error VFDCONFIG (default configuration name) must be set
#endif


// ============================================================================
// 
//   Helper functions
// 
// ============================================================================

static inline void Error(char *msg)
// ----------------------------------------------------------------------------
//    Report a problem of some sort
// ----------------------------------------------------------------------------
{
    perror(msg);
    exit(1);
}


static int Help2(char *msg1, char *msg2)
// ----------------------------------------------------------------------------
//   Show help
// ----------------------------------------------------------------------------
{
     if (msg1)
          printf("*** %s%s\n", msg1, msg2);
     printf("Usage: tvsetup [options]\n");
#define OPTION(name, usage, code)   printf("%15s: %s\n", #name, usage);
#define FLAG(name, usage, code)     printf("%15s: %s\n", "[no]" #name, usage);
#include VFDOPTIONS
     if (msg1)
          printf("*** %s%s\n", msg1, msg2);
     exit(0);
     return 0;
}


#define Help1(msg)      Help2(msg, "")
#define Help()          Help2(NULL, "")


static int force_x = -1;
static int force_y = -1;
static int force_w = -1;
static int force_h = -1;
static int force_p = -1;
static char *server = "localhost";
static int port = 12568;
static char *cfg = VFDCONFIG; 
static int verbose = 0;

#define dprintf verbose && printf


// ============================================================================
// 
//   Client-side VFD API
// 
// ============================================================================

static void vfd_options(int argc, char **argv)
// ----------------------------------------------------------------------------
//   Process options for the VFD parameters
// ----------------------------------------------------------------------------
{
     // Parse options
     int arg;
     for (arg = 1; arg < argc; arg++)
     {
          char *opt = argv[arg];
          int boolval = 0;

          // Skip '-' in front of options.
          while (opt[0] == '-')
               opt++;

#define OPTION(name, usage, code)               \
          if (!strcmp(opt, #name)) { code; } else
#define FLAG(name, usage, code)                                 \
         if (!strcmp(opt, #name)) { boolval = 1; code; } else   \
         if (!strcmp(opt, "no" #name)) { boolval = 0; code; } else
#define BOOL    boolval
#define INT     ((++arg < argc) ? atol(argv[arg]) : Help2("Missing int value", argv[arg]))
#define STRING  ((++arg < argc) ? argv[arg] : (Help2("Missing string", argv[arg]),""))
#include VFDOPTIONS

          Help2 ("Unknown option:", boolval ? argv[arg] : argv[arg]);
     }
}


static int vfd_open(char *server_name, int port)
// ----------------------------------------------------------------------------
//   Open the VFD on the given server and port
// ----------------------------------------------------------------------------
{
    int sockfd,n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
         Error(VFDCLIENT "(socket)");
    server = gethostbyname(server_name);
    if (server == NULL)
         Error(VFDCLIENT "(gethostbyname)");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        Error(VFDCLIENT "(connect)");

    // Check who we are talking to
    if (write(sockfd, "V", 1) < 0)
         Error(VFDCLIENT "(open-write)");
    bzero(buffer, sizeof(buffer));
    n = read(sockfd, buffer, sizeof(buffer)-1);
    if (n < 0)
         Error(VFDCLIENT "(open-read)");
    if (!strstr(buffer, "Ready"))
    {
         fprintf(stderr, "Protocol error: Got '%s'\n", buffer);
         Error(VFDCLIENT "(vfd800 protocol)");
    }
    if (!strstr(buffer, "VFD800 Server"))
    {
         n = read(sockfd, buffer, sizeof(buffer)-1);
         if (n < 0)
              Error(VFDCLIENT "(open-read)");
         if (!strstr(buffer, "VFD800 Server"))
         {
              fprintf(stderr, "Protocol error(2): Got '%s'\n", buffer);
              Error(VFDCLIENT "(vfd800 protocol2)");
         }
    }

    return sockfd;
}


static void vfd_cmd(int fd, const char *fmt, ...)
// ----------------------------------------------------------------------------
//   Send a formatted message onto a VFD socket
// ----------------------------------------------------------------------------
{
     char buffer[256];
     va_list va;
     va_start(va, fmt);
     bzero(buffer, sizeof(buffer));
     vsnprintf(buffer, sizeof(buffer)-1, fmt, va);
     va_end(va);
     if (write(fd, buffer, strlen(buffer)) < 0)
          Error(VFDCLIENT "(cmd-write)");
}


static void vfd_printf(int vfd, int x, int y, const char *fmt, ...)
// ----------------------------------------------------------------------------
//   Send a formatted message as text onto a VFD
// ----------------------------------------------------------------------------
{
     char buffer[256];
     char *ptr;
     va_list va;
     va_start(va, fmt);
     bzero(buffer, sizeof(buffer));
     vsnprintf(buffer, sizeof(buffer)-1, fmt, va);
     va_end(va);
     ptr = strchr(buffer, '\n');
     if (ptr) *ptr = 0;
     vfd_cmd(vfd, "X%dY%dT", x, y);
     if (write(vfd, buffer, strlen(buffer)) < 0 ||
         write(vfd, "\n", 1) < 0)
          Error(VFDCLIENT "(printf-write)");
}


typedef struct vfd_glyph_extent
// ----------------------------------------------------------------------------
//   Information about the size of a glyph
// ----------------------------------------------------------------------------
{
     int w;
     int h;
} vfd_glyph_extent;


static vfd_glyph_extent vfd_printf_area(int vfd, const char *fmt, ...)
// ----------------------------------------------------------------------------
//   Compute the area occupied by a given text
// ----------------------------------------------------------------------------
{
     char buffer[256];
     char *ptr;
     vfd_glyph_extent extent;

     va_list va;
     va_start(va, fmt);
     bzero(buffer, sizeof(buffer));
     vsnprintf(buffer, sizeof(buffer)-1, fmt, va);
     va_end(va);
     ptr = strchr(buffer, '\n');
     if (ptr) *ptr = 0;
     if (write(vfd, "A", 1) < 0 ||
         write(vfd, buffer, strlen(buffer)) < 0 ||
         write(vfd, "\n", 1) < 0)
          Error(VFDCLIENT "(printf-area-write)");
     if (read(vfd, buffer, sizeof(buffer)) < 0)
          Error(VFDCLIENT "(printf-area-read)");
     extent.w = extent.h = 0;
     sscanf(buffer, "W%i H%i", &extent.w, &extent.h);
     return extent;
}


typedef struct vfd_configuration
// ----------------------------------------------------------------------------
//   Description of a VFD configuration
// ----------------------------------------------------------------------------
{
     int x, y, w, h;
     int priority;
} vfd_configuration;


static inline vfd_configuration vfd_configure (int vfd, char *name)
// ----------------------------------------------------------------------------
//   Send a configuration request, and get back coordinates
// ----------------------------------------------------------------------------
{
     vfd_configuration cfg;
     char buffer[256];
     int n;
     vfd_cmd(vfd, "N%s\n", name);
     bzero(buffer, sizeof(buffer));
     n = read(vfd, buffer, sizeof(buffer)-1);
     if (n < 0)
          Error(VFDCLIENT "(configuration-read)");
     cfg.x = cfg.y = cfg.w = cfg.h = cfg.priority = 0;
     sscanf(buffer, "X%i Y%i W%i H%i P%i",
            &cfg.x, &cfg.y, &cfg.w, &cfg.h, &cfg.priority);

     // Check for local overrides
     if (force_x >= 0 || force_y >= 0 ||
         force_w >= 0 || force_h >= 0 || force_p >= 0)
     {
          if (force_x >= 0)
               cfg.x = force_x;
          if (force_y >= 0)
               cfg.y = force_y;
          if (force_w >= 0)
               cfg.w = force_w;
          if (force_h >= 0)
               cfg.h = force_h;
          if (force_p >= 0)
               cfg.priority = force_p;

          // Send updated area to the server
          vfd_cmd(vfd, "M+CX%dY%dW%dH%dZ%dFRD",
                  cfg.x, cfg.y, cfg.w, cfg.h, cfg.priority);
     }          

     return cfg;
}


static inline void vfd_update(int vfd)
// ----------------------------------------------------------------------------
//   Request a screen update
// ----------------------------------------------------------------------------
{
     if (write(vfd, "U", 1) < 0)
          Error(VFDCLIENT "(update)");
}


static inline void vfd_clear(int vfd)
// ----------------------------------------------------------------------------
//   Clear the screen
// ----------------------------------------------------------------------------
{
     if (write(vfd, "C", 1) < 0)
          Error(VFDCLIENT "(clear)");
}


static inline void vfd_select_mask(int vfd)
// ----------------------------------------------------------------------------
//   Select the mask for the next drawing operations
// ----------------------------------------------------------------------------
{
     if (write(vfd, "M", 1) < 0)
          Error(VFDCLIENT "(select_mask)");
}


static inline void vfd_select_display(int vfd)
// ----------------------------------------------------------------------------
//   Select the display contents for next drawing operations
// ----------------------------------------------------------------------------
{
     if (write(vfd, "D", 1) < 0)
          Error(VFDCLIENT "(select_display)");
}


static inline void vfd_set_color(int vfd, int color)
// ----------------------------------------------------------------------------
//   Select color: 1 = bright, 0 = dark
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "%c", color ? '+' : '-');
}


static inline void vfd_set_brightness(int vfd, float brightness)
// ----------------------------------------------------------------------------
//   Select color: 1 = bright, 0 = dark
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "B%d", (int) (brightness * 255));
}


static inline void vfd_reverse(int vfd, int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Invert the selected area
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%d*", x, y, w, h);
}


static inline void vfd_set_priority(int vfd, int priority)
// ----------------------------------------------------------------------------
//   Select client priority (higher = more visible)
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "Z%d", priority);
}


static inline void vfd_rectangle(int vfd,
                                int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Draw a rectangle outline
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%dOR", x, y, w, h);
}


static inline void vfd_fill_rectangle(int vfd,
                                      int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Draw a filled rectangle
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%dFR", x, y, w, h);
}


static inline void vfd_ellipse(int vfd,
                               int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Draw an ellipse outline
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%dOE", x, y, w, h);
}


static inline void vfd_fill_ellipse(int vfd,
                                    int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Draw a filled ellipse
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%dFE", x, y, w, h);
}


static inline void vfd_point(int vfd, int x, int y, int w, int h)
// ----------------------------------------------------------------------------
//   Plot a single pixel
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dP", x, y);
}


static inline void vfd_line(int vfd, int x, int y, int dx, int dy)
// ----------------------------------------------------------------------------
//   Draw a line
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "X%dY%dW%dH%dL", x, y, dx, dy);
}


static void vfd_define_glyph(int vfd, int c, int w, int h, char *bytes)
// ----------------------------------------------------------------------------
//    Send a glyph definition to the server
// ----------------------------------------------------------------------------
{
     int n, sz = w * h;
     vfd_cmd(vfd, "G %d %d %d ", c, w, h);
     for (n = 0; n < sz; n++)
         vfd_cmd(vfd, "%02X", (unsigned char) *bytes++);
}


static vfd_glyph_extent vfd_text_to_glyph(int vfd, int c, char *str)
// ----------------------------------------------------------------------------
//    Create a glyph from a combination of glyphs
// ----------------------------------------------------------------------------
{
     char buffer[256];
     int n;
     vfd_glyph_extent extent;

     vfd_cmd(vfd, "I%d:%s\n", c, str);

     n = read(vfd, buffer, sizeof(buffer)-1);
     if (n < 0)
          Error(VFDCLIENT "(text2glyph-read)");
     extent.w = extent.h = 0;
     sscanf(buffer, "W%i H%i", &extent.w, &extent.h);

     return extent;
}


static void vfd_select_font(int vfd, int font)
// ----------------------------------------------------------------------------
//    Select the fontset for this client (0 = mono, 1 = proportional)
// ----------------------------------------------------------------------------
{
     vfd_cmd(vfd, "J%d", font);
}


#endif /* VFDCLIENT_H */
